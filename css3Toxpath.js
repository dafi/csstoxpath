var fs = require('fs');
var cssToXPath = require('css-to-xpath');

var obj = JSON.parse(fs.readFileSync(process.argv[2], 'utf8'));

var attrsToConvert = [
	"image",
   	"title",
  	"container",
   	"multiPage"];

var selectors = obj.selectors

function canConvert(v) {
	return v && v.indexOf('pageSel:') < 0 && v.indexOf('regExp:') < 0
}

for (var i in selectors) {
	var sel = selectors[i];

	for (var a = 0; a < attrsToConvert.length; a++) {
		var attr = attrsToConvert[a];
		if (canConvert(sel[attr])) {
			sel[attr] = cssToXPath(sel[attr]);
		}
	}
}

console.log(JSON.stringify(obj, null, 2));